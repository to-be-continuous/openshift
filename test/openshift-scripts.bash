#!/usr/bin/env bash

# load CF scripts from template
scripts=/tmp/cf-scripts.sh
echo "#!/bin/bash" > $scripts
sed -n '/BEGSCRIPT/,/ENDSCRIPT/p' "$(dirname ${BASH_SOURCE[0]})/../templates/gitlab-ci-openshift.yml" >> $scripts
source $scripts

# define oc cli mock
function oc() {
    nb_results=${#OC_RESULT[@]}
    if [[ ${nb_results} -ne 0 ]]
    then
        result=${OC_RESULT[0]}
        if [[ ${nb_results} -eq 1 ]]
        then
            OC_RESULT=()
        else
            OC_RESULT=${OC_RESULT[@]:1:$nb_results}
        fi
        echo "$result"
    else
        echo "oc $*"
    fi
}

# define env mock
function env() {
    cat $ENV_FILE
}

# export mock to make visible to child scripts
export -f oc env
