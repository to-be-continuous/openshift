# [5.4.0](https://gitlab.com/to-be-continuous/openshift/compare/5.3.1...5.4.0) (2025-02-22)


### Features

* add unofficial oc extra options support ([b322e12](https://gitlab.com/to-be-continuous/openshift/commit/b322e12775c8a00dd763c9b27e9e6608324b0ffc))

## [5.3.1](https://gitlab.com/to-be-continuous/openshift/compare/5.3.0...5.3.1) (2025-02-01)


### Bug Fixes

* homogenize new TBC envsubst mechanism ([1a9dfb4](https://gitlab.com/to-be-continuous/openshift/commit/1a9dfb416e1195c04b4bf42702d17a071a430e00))

# [5.3.0](https://gitlab.com/to-be-continuous/openshift/compare/5.2.3...5.3.0) (2025-01-27)


### Features

* disable tracking service by default ([bce1c4a](https://gitlab.com/to-be-continuous/openshift/commit/bce1c4a99b9cb7d9510c4fc1d1a56f55f2d3a4e0))

## [5.2.3](https://gitlab.com/to-be-continuous/openshift/compare/5.2.2...5.2.3) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([91c598f](https://gitlab.com/to-be-continuous/openshift/commit/91c598f1dcd08cff1253f6672731cd325c9a68a8))

## [5.2.2](https://gitlab.com/to-be-continuous/openshift/compare/5.2.1...5.2.2) (2024-04-03)


### Bug Fixes

* **vault:** use vault-secrets-provider's "latest" image tag ([74ca9a9](https://gitlab.com/to-be-continuous/openshift/commit/74ca9a9750e63d9254740110bc5de455f9fbbf73))

## [5.2.1](https://gitlab.com/to-be-continuous/openshift/compare/5.2.0...5.2.1) (2024-1-30)


### Bug Fixes

* sanitize empty variable test expressions ([617edd2](https://gitlab.com/to-be-continuous/openshift/commit/617edd2da72887bf58b29fddb397551453dff6a2))

# [5.2.0](https://gitlab.com/to-be-continuous/openshift/compare/5.1.0...5.2.0) (2024-1-27)


### Features

* migrate to CI/CD component ([3670d4a](https://gitlab.com/to-be-continuous/openshift/commit/3670d4a0adcc9e560bca9fcc9df8e00a71750cbd))

# [5.1.0](https://gitlab.com/to-be-continuous/openshift/compare/5.0.1...5.1.0) (2023-12-8)


### Features

* use centralized service images (gitlab.com) ([989589c](https://gitlab.com/to-be-continuous/openshift/commit/989589c9da53c8fb6abec420c2bf2461ad57ed19))

## [5.0.1](https://gitlab.com/to-be-continuous/openshift/compare/5.0.0...5.0.1) (2023-12-2)


### Bug Fixes

* envsubst when variable contains a '&' ([73a3b17](https://gitlab.com/to-be-continuous/openshift/commit/73a3b1726cc7a344c60f925fa1ad77fc142977c2))

# [5.0.0](https://gitlab.com/to-be-continuous/openshift/compare/4.1.0...5.0.0) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([5aa7a94](https://gitlab.com/to-be-continuous/openshift/commit/5aa7a942bad2eb13428c4b8b359f29475ca007ab))


* feat!: support environment auto-stop ([08d32f5](https://gitlab.com/to-be-continuous/openshift/commit/08d32f5fcca6003036beadf7583d4911de984b4c))


### BREAKING CHANGES

* now review environments will auto stop after 4 hours
by default. Configurable (see doc).

# [4.1.0](https://gitlab.com/to-be-continuous/openshift/compare/4.0.0...4.1.0) (2023-09-02)


### Features

* allow propagate custom output variables ([99c2006](https://gitlab.com/to-be-continuous/openshift/commit/99c200649939fb1e363a1c5f4416a04abee04684))

# [4.0.0](https://gitlab.com/to-be-continuous/openshift/compare/3.1.0...4.0.0) (2023-08-28)


### Features

* **oidc:** OIDC authentication support now requires explicit configuration (see doc) ([2e65130](https://gitlab.com/to-be-continuous/openshift/commit/2e65130e175d888bf02ccd438af5c0593e4df7b1))


### BREAKING CHANGES

* **oidc:** OIDC authentication support now requires explicit configuration (see doc)

# [3.1.0](https://gitlab.com/to-be-continuous/openshift/compare/3.0.0...3.1.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([25b5988](https://gitlab.com/to-be-continuous/openshift/commit/25b59884497a4ea94eec97c7551e30850bd5fc76))

# [3.0.0](https://gitlab.com/to-be-continuous/openshift/compare/2.2.5...3.0.0) (2023-04-05)


### Features

* **deploy:** redesign deployment strategy ([989340e](https://gitlab.com/to-be-continuous/openshift/commit/989340e677e34e65e69a63501595faa7b9157805))


### BREAKING CHANGES

* **deploy:** $AUTODEPLOY_TO_PROD no longer supported (replaced by $OS_PROD_DEPLOY_STRATEGY - see doc)

## [2.2.5](https://gitlab.com/to-be-continuous/openshift/compare/2.2.4...2.2.5) (2023-03-20)


### Bug Fixes

* rename output dotenv to prevent collision with input dotenv file ([4c63444](https://gitlab.com/to-be-continuous/openshift/commit/4c634440f155e588ad0d1ca6ad9deab99a9fd793))

## [2.2.4](https://gitlab.com/to-be-continuous/openshift/compare/2.2.3...2.2.4) (2023-02-13)


### Bug Fixes

* **readiness:** call after url is determined; example uses $environment_url ([e666a49](https://gitlab.com/to-be-continuous/openshift/commit/e666a49d9d5c81774ca3d66d61d25d26990dfcf7))

## [2.2.3](https://gitlab.com/to-be-continuous/openshift/compare/2.2.2...2.2.3) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([23edeeb](https://gitlab.com/to-be-continuous/openshift/commit/23edeeb7a8eb2bda160bf79fb9518ecc14f5f107))

## [2.2.2](https://gitlab.com/to-be-continuous/openshift/compare/2.2.1...2.2.2) (2022-12-17)


### Bug Fixes

* hanging awk script ([8364533](https://gitlab.com/to-be-continuous/openshift/commit/836453344ff644a9dda2bc40818599925dc40dd9))

## [2.2.1](https://gitlab.com/to-be-continuous/openshift/compare/2.2.0...2.2.1) (2022-12-16)


### Bug Fixes

* add missing awkenvsubst function ([c46527d](https://gitlab.com/to-be-continuous/openshift/commit/c46527d1f287fe77534d7037b197d2c1633ae6a8))

# [2.2.0](https://gitlab.com/to-be-continuous/openshift/compare/2.1.0...2.2.0) (2022-12-15)


### Features

* support dynamic env url ([0a00eff](https://gitlab.com/to-be-continuous/openshift/commit/0a00eff96bafe0c4b44c62c49003d0b63c2db855))

# [2.1.0](https://gitlab.com/to-be-continuous/openshift/compare/2.0.0...2.1.0) (2022-12-13)


### Features

* **vault:** configurable Vault Secrets Provider image ([ba0cfa0](https://gitlab.com/to-be-continuous/openshift/commit/ba0cfa098242b72012fdf75940d7feb968cbb4b5))

# [2.0.0](https://gitlab.com/to-be-continuous/openshift/compare/1.4.0...2.0.0) (2022-08-05)


### Features

* make MR pipeline the default workflow ([67b079f](https://gitlab.com/to-be-continuous/openshift/commit/67b079fc9c05e4e3e0a40de796bc5e89a2df9699))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [1.4.0](https://gitlab.com/to-be-continuous/openshift/compare/1.3.0...1.4.0) (2022-06-30)


### Features

* enforce AUTODEPLOY_TO_PROD and PUBLISH_ON_PROD as boolean variables ([2769f23](https://gitlab.com/to-be-continuous/openshift/commit/2769f231c2095bb551a9cb57e048e36d77f55d21))

# [1.3.0](https://gitlab.com/to-be-continuous/openshift/compare/1.2.7...1.3.0) (2022-05-01)


### Features

* configurable tracking image ([841323c](https://gitlab.com/to-be-continuous/openshift/commit/841323ccf2e9f1f1f930b32ca051808d8b980b21))

## [1.2.7](https://gitlab.com/to-be-continuous/openshift/compare/1.2.6...1.2.7) (2022-02-24)


### Bug Fixes

* **vault:** revert Vault JWT authentication not working ([d82538b](https://gitlab.com/to-be-continuous/openshift/commit/d82538bf1013911267b6717106140a91e3e39119))

## [1.2.6](https://gitlab.com/to-be-continuous/openshift/compare/1.2.5...1.2.6) (2022-02-23)


### Bug Fixes

* **vault:** Vault JWT authentication not working ([18102f8](https://gitlab.com/to-be-continuous/openshift/commit/18102f89a764b1684bf79224ceacdd7e45ec8c01))

## [1.2.5](https://gitlab.com/to-be-continuous/openshift/compare/1.2.4...1.2.5) (2022-01-10)


### Bug Fixes

* non-blocking warning in case failed decoding [@url](https://gitlab.com/url)@ variable ([44c9747](https://gitlab.com/to-be-continuous/openshift/commit/44c9747922c6f7ab1c73a42dbe5bc9270ba99e29))

## [1.2.4](https://gitlab.com/to-be-continuous/openshift/compare/1.2.3...1.2.4) (2021-12-03)


### Bug Fixes

* execute hook scripts with shebang shell ([37bb630](https://gitlab.com/to-be-continuous/openshift/commit/37bb63079d102a6905041738ff0256f39c77627f))

## [1.2.3](https://gitlab.com/to-be-continuous/openshift/compare/1.2.2...1.2.3) (2021-10-07)


### Bug Fixes

* use master or main for production env ([d97a80c](https://gitlab.com/to-be-continuous/openshift/commit/d97a80ca9eb9dcc2330a46a585d3430cd4bc5bf5))

## [1.2.2](https://gitlab.com/to-be-continuous/openshift/compare/1.2.1...1.2.2) (2021-09-08)

### Bug Fixes

* change variable behaviour ([2228f01](https://gitlab.com/to-be-continuous/openshift/commit/2228f01c3c2b196b476a9ba9ba7a87746aee3533))

## [1.2.1](https://gitlab.com/to-be-continuous/openshift/compare/1.2.0...1.2.1) (2021-07-08)

### Bug Fixes

* conflict between vault and scoped vars ([46f2b7d](https://gitlab.com/to-be-continuous/openshift/commit/46f2b7de6b2749faa31491239aa72b73b6ead079))

## [1.2.0](https://gitlab.com/to-be-continuous/openshift/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([4e63782](https://gitlab.com/to-be-continuous/openshift/commit/4e6378295faaa8b7a45e6f43083a60b209c3a17f))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/openshift/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([c6ce696](https://gitlab.com/Orange-OpenSource/tbc/openshift/commit/c6ce696983831e340a3c0f16dbc78479c3e02c91))

## 1.0.0 (2021-05-06)

### Features

* initial release ([95a6b01](https://gitlab.com/Orange-OpenSource/tbc/openshift/commit/95a6b01b01d4161b5a09d9da32bf34039d77feb3))
